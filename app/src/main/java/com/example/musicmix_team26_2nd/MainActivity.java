package com.example.musicmix_team26_2nd;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.os.Handler;

public class MainActivity extends AppCompatActivity {
    private ImageView imageView;
    long animationDuration = 1000;//ms
//    final Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (ImageView)findViewById(R.id.imageView);
    }
    public void handleAnimation(View view) {

        ObjectAnimator animatorX = ObjectAnimator.ofFloat(imageView,"x",420f);
        animatorX.setDuration(animationDuration);

        ObjectAnimator animatorY = ObjectAnimator.ofFloat(imageView, "y", 300f);
        animatorY.setDuration(animationDuration);

        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(imageView, View.ALPHA, 1.0f, 0.0f);
        alphaAnimation.setDuration(animationDuration);

        ObjectAnimator rotateAnimation = ObjectAnimator.ofFloat(imageView, "rotation", 0f, 360f);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(alphaAnimation, rotateAnimation);
        animatorSet.start();
        configureNextButton();
    }

    private void configureNextButton()
    {
        ImageView nextButton = (ImageView) findViewById(R.id.imageView);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,selectSong.class));
            }
        });
    }}